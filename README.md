# PSYC 306: Research Methods

This repository contains lecture materials and syllabi for the Research Methods (both 6-week and 15-week formats) taught by Jeremy R. Winget, as part of the psychology major undergraduate curriculum at Loyola University Chicago.