<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Quasi-experimental Research Designs</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 306, Winget" />
    <script src="libs/header-attrs-2.8/header-attrs.js"></script>
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Quasi-experimental Research Designs
### PSYC 306, Winget

---




#  Quasi-experimental designs 

+ Experimenters have only partial control over independent variables

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image1.png" width=100%&gt;]

???

+ Cannot control manipulation OR cannot randomly assign
+ Done when wanting to maximize both internal and external validity
+ By their nature, higher in external validity compared to true experiments
+ However, sometimes concerns of internal validity (often addressed by identifying meaningful control groups)

---
##  Reasons to use quasi-experimental designs 

+ Some variables cannot be manipulated
	+ Gender, income, age
	+ Self-esteem, depression
	+ Ethical considerations


+ Generally increase external validity
	+ Must pay attention to concerns for internal validity

???

+ Some variables simply cannot be manipulated, yet they have extremely important consequences
+ So we don't allow our inability to study them using random assignment to stand in the way of studying them at all
+ Other variables *can* be manipulated are still very difficult to study experimentally (e.g., expensive equipment, lack of resources)
+ Sometimes we're forced to study phenomena naturally rather than in the lab
+ Even when random assignment can be used, sometimes it *shouldn't be* for ethical reasons (HIV/AIDS)
+ If researchers manipulated variables that are thought to influence physical (or emotional) well-being, they could be randomly assigning some people to conditions that lead to serious health problems

---
#  Types of quasi-experimental designs 

+ Person-by-treatment


+ Natural experiment


+ Nature and treatment


+ Patched-up

???

+ While true experiments are often the ideal, many researchers love using quasi-experiment designs because they can be used when true experiments cannot
+ They're best thought of as hybrid designs that combine many of the desirable features of both experimental and nonexperimental research designs
+ Sometimes they do this by bringing measured rather than manipulated variables to the lab
+ Other times, they do so by allowing researchers to investigate a naturally occurring event that approximates an experimental manipulation
+ In other words, there are different kinds of quasi-exp designs

---
#  Person-by-treatment designs 

+ Measures one independent variable and manipulates another one
	+ Very similar to a true experiment, except that one variable is out of your control
	+ May want to use prescreening to select participants who differ on the measured variable

???

+ In social, personality, and clinical psych, the most common kind of quasi-exp is probably the person-by-treatment quasi-exp
+ Almost always take place in the lab (because of the manipulation component; use of random assignment to gain control over 1 IV)
+ For example, clinical researcher interested in depression might identify patients at a clinic who are interested in taking part in laboratory research
+ Participants only qualify for the study if they clearly do or do not meet the diagnostic criteria for a major depressive episode 
+ Depression is the measured variable
+ Once depressed and nondepressed participants arrive in the lab, they might be asked to perform an experimental task designed to put them in a pos/neg mood
+ Randomly assignment independent of their level of depression
+ DV could be explanation offered for neg life events
+ Might see depressed and nondepressed participants explain neg events differently only when in an experimentally induced neg mood

---
#  Prescreening 

&lt;br&gt;
.pull-left[
+ Median split
	+ Problem: People near the cutoff are more similar to one another than they are to the extreme members of their own group


+ Extreme groups
	+ Upper and lower 25% of pre-tested participants
]

&lt;br&gt;
&lt;br&gt;
.pull-right[&lt;img src="assets/img/image2.png" width=100%&gt;]

???

+ Such designs often make use of a procedure called prescreening, which means researchers are screening participants on an inidivd. diff. measure prior to running the lab study
+ Helps to create two groups on the measured variable
+ Prevents the measured variable from giving away the hypothesis or the manipulation from influencing the measured variable
+ 2 common procedures for doing this (there are others though)
+ Extreme groups may be more representative of the groups you are trying to create, but you will always loose 50% of the data you collect

---
#  Natural experiments 

+ Use a naturally occurring manipulation
	+ Events that are fairly arbitrary, but not completely random


+ Experimenter has no control over the variables


+ Must establish that groups are comparable
	+ Measure and control for confounds

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image3.png" width=80%&gt;]

???

+ In person-by-treatment, researcher maintains partial control by measuring and manipulating
+ Thus, control over at least one IV
+ In natural experiments, researcher does not use random assignment at all; thus, relinquishes experimental control completely
+ All variables are simply measured, but they involve naturally occurring “manipulations”
+ For example, consider a natural disaster. In any given year, many people in the Great Plains or SE US will have homes damaged by tornadoes.
+ However, many of their neighbors will escape the same fate.
+ Although people in these regions will differ from those in the north, it is unlikely tornadoes seek out one type of resident over another
+ Thus, within these regions, tornadoes affect a mostly random group of people
+ It would therefore be reasonable for researchers to see if people who have their home struck by tornadoes experience higher rates of anxiety/depression/ptsd than their spared neighbors
+ In this way, tornado exposure acts as a naturally occurring IV while anxiety/depression outcomes serve as DVs
+ Now, strictly speaking, natural occurring manipulations do not occur *completely* at random (e.g., tornadoes are more likely in certain regions than others)
+ A quasi-exp studying the effects within a small geographic area doesn't have much to be concerned about
+ But one comparing victims in different counties does
+ For example:
    + Seat belt laws vary by state and the evidence says traffic deaths are reduced in states that require wearing seat belts
    + In truth, however, third variables may be at work
    + Seat-belt states might be passed in states with stricter than usual speed limits or states that are cracking down on drunk driving
    + In order to treat these as manipulations, researchers must make sure the groups are comparable (essentially what random assignment does in a true experiment)
    + In this case, need to demonstrate states that did and did not pass seat belt laws had comparable traffic fatality rates just prior to the time they adopted the new laws
    + This would help a lot, but researchers would still need to be sure nothing else changed at the same time the seat belt laws changed.
+ **Note, natural experiments are different than the naturalistic observation studies we discussed last week**

---
#  Natural experiments 

&lt;br&gt;
.pull-left[
+ Loma Prieta Earthquake Study
	+ Nolen-Hoeksema &amp; Morrow, 1991


+ IV 1:
	+ Impacted by earthquake or not


+ IV 2:
	+ Engaged in rumination vs. distraction


+ DV:
	+ Depression 7 weeks later
]

.pull-right[&lt;img src="assets/img/image4.jpeg" height=400&gt;]

???

+ Testing effects of natural disaster on anxiety/depression
+ Measures of emotional health and styles of responding to negative moods were obtained for 137 students 14 days before the Loma Prieta earthquake
+ Follow-ups were done 10 days after and 7 weeks after the earthquake to test predictions about which of the students would show the most enduring symptoms of depression and posttraumatic stress. 
+ Regression analysis showed that students who, before the earthquake, already had elevated levels of depression, stress symptoms, and a ruminative style of responding to their symptoms had more depression and stress symptoms for both follow-ups. 
+ Students who were exposed to more dangerous or difficult circumstances because of the earthquake also had elevated symptom levels 10 days after the earthquake. 
+ Similarly, students who, during the 10 days after the earthquake, had more ruminations about the earthquake were still more likely to have high levels of depressive and stress symptoms 7 weeks after the earthquake.

---
#  Natural experiments 

&lt;br&gt;
.pull-left[
+ Capilano Bridge Study
	+ Dutton &amp; Aron, 1974


+ IV:
	+ Rickety vs. stable bridge


+ DV:
	+ Number of men who called the researcher for a date


+ Results:
	+ Significantly more men called who went across the rickety vs. the stable bridge
]

.pull-right[.center[&lt;img src="assets/img/image5.jpeg" height=400&gt;]]

???

+ Testing emotion excitation (general arousal can be misattributed to the wrong emotion)
+ Male passersby were contacted on either a fear-arousing suspension bridge or a non-fear-arousing bridge by an attractive female interviewer who asked them to fill out questionnaires containing ambiguous pictures
+ Sexual content of the stories about the pictures written by the participants on the fear-arousing bridge and tendency of these participants to attempt postexperimental contact with the interviewer were significantly higher than those on the non-fear-arousing bridge 
+ No significant differences between bridges were obtained for male interviewers

---
#  Nature and treatment designs 

+ Two naturally occurring groups are given different treatments


+ Only useful if:
	+ Two groups were similar before treatment
	+ Two groups experienced different treatments, but nothing else was different

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image6.jpeg" width=400&gt;]

???

+ These last 2 quasi-exp do not cover all possible quasi-exp designs
+ However, these other designs are a bit less common in practice
+ Nevertheless, they can be useful (especially patched-up designs) and they provide a clearer picture of quasi-exp applications
+ One kind of quasi-exp that is probably more useful in applied settings (i.e., non-lab studies, practical problems) is roughly the opposite of the person-by-treatment design
+ Sometimes we want to study people who have already sorted themselves into natural groups that cannot be pulled apart
+ These groups are people are sometimes highly similar
+ So rather than exposing each group to both levels of a manipulation, a researcher might expose the each group to a different level of the manipulation
+ *natural groups with experimental treatment* 
+ Comparing vegetarians to pescatarians

---
#  Comparability 

+ Finding a comparable control group is difficult
	+ Want another group that is similar in all ways except the treatment

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image7.jpeg" width=65%&gt;]

???

+ For example, suppose a meteor were to strike a rural Midwest farming community
+ Suppose it also wipes out a large grain factory, dealing huge blow to the local economy
+ Now, this even is likely random (it could have hit anywhere on the Earth), so we can treat it as a natural manipulation
+ However, let's suppose a group of psychologists came along in the following month to see if the meteor increased levels of PTSD
+ They would have a big problem: they would need to find a reasonable comparison group
+ If we used the county to the west as a comparison group, we would need to be conscious of:
    1. Carryover effects (does that county also rely on the grain factory for part of their economy?)
    1. Demographics (does one of the counties have a large Amish population for instance?)
    1. State lines/jurisdictions (social, economic, and political changes)
+ What would you use as a control group for:
    1. Loyola freshmen? (DePaul freshman)
    1. People with depression? (median split or extreme groups)
+ It's sometimes so difficult to find a comparison group, researchers developed the notion of patching…

---
#  Patched-up designs 

+ Quasi-experimental design with added control groups


+ Patching
	+ Adding new conditions to a study
	+ Used to test specific concerns about an original study

&lt;br&gt;
.center[&lt;img src="assets/img/image8.jpeg" width=475&gt;]

???

+ All else being equal – chapter 7 – core principle behind true experiments
+ If you want to show an IV is influencing a DV, must hold everything else but the IV constant across conditions
+ As we know, this requires random assignment (deals with person confounds) and procedural control (operational confounds)
+ This high level of control not possible with quasi-exp, so we must use a different method to uncover the meaning of our treatment effects
+ This method is called patching
+ Patching occurs when a researcher adds new conditions to a study to help establish the size of a quasi-exp effect, to test for the influence of confounds, or both
+ The end result is often referred to as a patched-up design
+ Patched-up designs occur when researchers continually add control groups to a quasi-exp design
+ In the end, a patched-up design will resemble a patchwork quilt, with many different conditions sewn into the original design, each to deal with specific concerns
+ When using this approach, researchers must think of all possible control groups that should be included to clarify the meaning of any/all effects that are found

---
#  Patched-up designs 

+ Common types
	+ One-group
	+ One-group pretest-posttest
	+ Posttest-only with nonequivalent groups
	+ Pretest-posttest with nonequivalent groups
	+ Time-series
	+ Internal analyses

???

+ One-group: all participants are in group group (the treatment group; aka pseudo-experiment)
+ One-group pretest-posttest: one group, measure before and after manipulation/treatment
+ Posttest only: 2 groups, 1 given manipulation, measure, compare
+ Pretest-posttest: 2 groups measured, 1 given manipulation, both measured again, compare
+ Time-series: long runs of data (comparable to longitudinal design)
+ Internal analysis: used to pinpoint precise mechanism behind a finding
+ See the text for more details on these distinctions 

---
#  Three ways to examine a behavior 

+ Helping behavior
	+ Non-experimental?
	+ Quasi-experimental?
	+ Experimental?

???

+ Have students work in groups to come up with three ways of studying helping behavior
    1. non-experimental—case study or single-variable research
    1. quasi-experimental—compare genders in helping behavior or longitudinal design
    1. experimental—class project—manipulate diffusion of responsibility and target group membership
+ Write down answers, then we will share as a class

---
class: inverse, center, middle

# Final Paper Formatting

???

+ 

---
#  General formatting 

+ The paper should be in Times New Roman, 12pt font


+ The entire paper should be double spaced


+ Use 1 inch margins on all sides

???

+ 

---
#  APA paper overview 

+ Title page


+ Abstract


+ Introduction
  + *.red[Draft already written]*


+ Method
  + *.red[Draft already written]*


+ Results
  + *.red[Draft already written]*


+ Discussion
  + *.red[Draft already written]*


+ References


+ Figure(s)

???

+ Note that for the results draft, it was okay to have the figures inside the paper...but now they will need to be placed in their own section after the references
+ We'll talk more about this in a moment
+ Since we've already talked about the intro, methods, results, and discussion sections, we'll focus on these other pieces today

---
#  Title page 

+ Title should provide a description of the paper's topic
	+ 10-12 words


+ Author affiliation


+ Running head
  + Goes in the header of the word document
	+ Maximum of 50 characters
	+ On title page only, this is preceded by the words “Running head:”
	+ All caps

???

+ 

---
#  Abstract 

+ Maximum of 250 words


+ Not indented


+ Provides a brief summary of the contents of the paper
  + Introduction, methods, results, etc.

???

+ 

---
#  Main text 

+ Introduction has no heading
	+ Center the title of the paper at the top of the page


+ Method, Results, and Discussion each have a heading and continue on the same page as the preceding section
	+ Do not leave headings alone at the bottom of the page

???

+ 

---
#  References 

+ Starts on its own page


+ Each reference uses a hanging indent
  + First line is flush with the margin, any remaining lines are indented


+ See examples for proper APA style

???

+ 

---
#  Figures

+ Main effects will be bar graphs
	+ Interaction will be a line graph


+ Should have a caption below that fully explains what it shows
  + e.g., "Figure 1. *Statement of main effect*"


+ See examples for formatting

???

+ 

---
#  Purdue OWL (online writing lab) 

+ General format
	+ https://tinyurl.com/8j9pacp4


+ Sample paper
	+ https://tinyurl.com/4ccdn6x8
	+ There's also a second one posted on Sakai
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
