<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Statistical Power</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 306, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Statistical Power
### PSYC 306, Winget

---

#  Introduction

+ Hypothesis testing is an imperfect process
  + There's always a chance we're wrong about our conclusions (Type I and Type II errors)


+ Statistical power
  + Probability of rejecting the null hypothesis when, in fact, the null is false and the research hypothesis is true

???

+ We've talked about how hypothesis testing is an imperfect endeavor...we always leave some chance we're wrong or that the results are simply due to chance
+ The best we can do is reject the null hypothesis and feel reasonably confident we've made the right choice
+ But Type I and Type II errors are always possible...so we can never prove anything and we can never be 100% sure about our conclusions
+ Not trying to be a pessimist...but there are problems with the process that we need to be aware of
+ However, in today's lecture...we'll focus on a solution to this problem
+ And this solution revolves around the concept of statistical power
  + Probability of rejecting the null hypothesis when, in fact, the null is false and the research hypothesis is true
+ So, you may have a situation in which your research hypothesis is objectively true...but this is a separate issue from how likely it is that the hypothesis testing procedure will find that the research hypothesis is supported
  + This is a subtle difference when first hearing about it...but this nuance sits at the heart of the issue of statistical power

---
#  What is statistical power?

+ Allows us to say: 
  + ~~We're right! We rejected the null hypothesis, so the research hypothesis is definitely right!~~
  + Not only did we reject the null hypothesis, we can also be very confident that we made the correct decision given the high amount of power that we had in the design!


+ Power is considered *before* you actually conduct a study
  + **Definition:** Probability of rejecting the null hypothesis when, in fact, the null is false and the research hypothesis is true
  + **General idea:** The likelihood of detecting an effect if there's something to be found

???

+ With power, instead of asking whether we're correct or not in our hypothesis testing procedure...we instead ask how likely it is that we're able to find that we're correct (by rejecting the null hypothesis) if, in fact, the null hypothesis is false
+ In other words, instead of saying "we're right--we rejected the null hypothesis--the research hypothesis is definitely right"...we can say something like "not only did we reject the null hypothesis, we can also be very confident that we made the correct decision given the high amount of power that we had in the design"
+ I'll admit, this wording is nuanced...but it's clear and strong--and it matches the best we can do in making the case that we've done well in working with the hypothesis-testing procedure
+ Power is often considered before you conduct a study
+ It has less to do with whether the question you have is correct...and more to do with the research design and how well that methodology is designed to uncover any effects that actually exist
+ In lay terms, its basically the likelihood of detecting an effect if there's something to be found

---
#  Essential concepts

1. The null hypothesis, `\(H_0\)`
1. Alpha level, `\(\alpha\)`
1. Type I error
1. Type II error

???

+ Statistical power relies on 4 different concepts that we've already encountered

---
#  Review: Decision table

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;

|                          | `\(H_0\)` is true  | `\(H_0\)` is false |
|:------------------------:|:--------------:|:--------------:|
| **Reject `\(H_0\)`**         | .red[**Type I error**] &lt;br&gt; “False alarm” &lt;br&gt; Probability = `\(\alpha\)`  | .green[**Correct decision!**] &lt;br&gt; Correctly detected a real effect &lt;br&gt; Probability = `\((1 – \beta)\)` |
| **Fail to reject `\(H_0\)`** | .green[**Correct decision!**] &lt;br&gt; Correctly detected no effect &lt;br&gt; Probability = `\((1 – \alpha)\)` | .red[**Type II error**] &lt;br&gt; “Miss” &lt;br&gt; Probability = `\(\beta\)` |

???

+ Explain correct decisions
+ When `\(H_0\)` is true and you reject it, you make a Type I error
  + When there really is no effect, but the statistical test comes out significant by chance, you make a Type I error
  + When Ho is true, the probability of making a Type I error is called alpha (α)
  + This probability is the significance level associated with your statistical test
+ When `\(H_0\)` is false and you fail to reject it, you make a Type II error
  + When, in the population, there really is an effect, but your statistical test comes out non-significant, due to inadequate power and/or bad luck with sampling error, you make a Type II error
  + When Ho is false, (so that there really is an effect there waiting to be found) the probability of making a Type II error is called beta (β)

---
#  An example of power

+ Let's say you work in the Office of Institutional Research (OIR)


+ Supervisor asks you to conduct a study to see if graduates from your college tend to score higher than the national average on the TMUJSI
  + `\(\mu = 25\)`
  + `\(\sigma = 5\)`


+ Money is tight, so you can only collect a random sample of 20 graduates
  + Use a one-tailed test, `\(\alpha = .05\)`
  + `\(\bar{X} = 26\)`
  

+ Does this hypothesis test even stand a chance?

???

+ Suppose you graduate with your degree and you get a job at a university's OIR
  + Office found on most college campuses that conducts research on issues related to the college (e.g., issues of grades, graduation rates, etc.)
+ Your supervisor comes to you ans says she wants you to conduct a study to see if graduates from your college tend to score higher than the national average on a standardized test of job satisfaction (TMUJSI - the made-up job satisfaction inventory)
+ This test has been given to millions of Americans and the best estimates of the population mean `\((\mu)\)` and standard deviation `\((\sigma)\)` are 25 and 5, respectively
+ Money is tight, and your supervisor tells you that there's only enough budget for you to collect data from 20 randomly sampled graduates
+ You're asked to use a standard alpha level of .05 and to assume a one-tailed test...with the research hypothesis being that graduates from your college score higher than the national average
+ So you collect the data from your 20 randomly sampled graduates, and compute the mean of the sample's TMUJST score and find it's 26
+ You pause...not knowing how your hypothesis test will go
+ You know that 26 is not much higher than 25, which was the national average
+ You wonder if your hypothesis test even has a fighting chance
+ Suppose the research hypothesis is true...that graduates from your college really do score higher than the national average
  + With an N of 20 and a difference between the means of only one unit, do you feel like your first task in this new job is going to succeed?
+ All of these questions pertain to the issue of estimating statistical power
  + Given all the information you have, **before you even conduct the hypothesis test**, you'd like to get a sense of how likely it is you'll reject the null hypothesis
+ So as you can see, power (even though it's somewhat abstract) is very applicable to real-world situations that matter

---
#  Conventions and decisions about power

+ Acceptable risk of a Type II error is often set at 1 in 5
  + A probability of 0.2 `\((\beta)\)`


+ The conventionally uncontroversial value for “adequate” statistical power is therefore set at `\(1 - 0.2 = 0.8\)` (or 80%)


+ People often regard the minimum acceptable statistical power for a proposed study as being an 80% chance of an effect that really exists showing up as a significant finding

???

+ 

---
#  Factors that influence power

+ One vs. two-tailed tests


+ Effect size (generally) and Cohen's d (in particular)


+ Population variability


+ Between- vs. within-subjects designs


+ Sample size

???

+ There are a number of things that influence power
+ One has to do with the specific form of hypothesis tests
+ Generally speaking, one tailed tests are more powerful than two tailed tests
  + Due to the fact that specific rejection regions are half as big with one-tail tests than two-tailed tests....because a two-tailed test divides the alpha across two rejection regions
+ Second, a larger effect size generally corresponds to more power
  + This is because large effect sizes indicate larger mean differences
  + And when mean differences are larger, the means between the comparison distributions are further apart...which makes it easier to detect an effect
+ Third, population variability can influence power by creating more noise between the means
  + In general, lower levels of population variability make correspond to more power
+ Fourth, the approach to the design you take can influence power
  + This is somewhat related to the idea of variability
  + Using between-subjects designes can create more noise between the groups...which makes detecting an effect more difficult
  + Within-subjects designs use each participant as their own control...which can reduce variability and increase power
+ Lastly, and probably the easiest to control, is the sample size
  + This is related to the fact that lower variability increases power
  + Recall that the larger the sample we collect, the lower the variability tends to be
  + This was a feature of sampling distributions and the central limit theorem
  + So as N increases, power tends to increase

---
## When do you need to compute statistical power?

+ A *prospective power analysis* is used before collecting data, to consider *design sensitivity*

--

+ A *retrospective power analysis* is used in order to know whether the studies you are interpreting were well enough designed
    

???

+ Should ALWAYS conduct a prospective power analysis when designing your study
+ Retrospective power analyses can be somewhat biased...so a better approach is to use something called a sensitivity analysis
+ Based on the features of your design (e.g., sample size collected, alpha level, etc.), it will tell you the smallest sample size you can reliably detect

---
## When do you need to compute statistical power?

+ In Cohen’s (1962) seminal power analysis of the journal of Abnormal and Social Psychology he concluded that over half of the published studies were insufficiently powered to result in statistical significance for the main hypothesis


+ Cohen, J. (1962). The statistical power of abnormal-social psychological research: A review. *Journal of Abnormal and Social Psychology*, *65*, 145-153.

???

+
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
