<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Hypothesis Testing</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 306, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Hypothesis Testing
### PSYC 306, Winget

---

#  Hypothesis testing &amp; inferential stats

+ There are a lot of conceptual pieces that go into hypothesis testing:
  + Probability &amp; z-scores
  + Constructing your research and null hypothesis
  + Comparison distribution
  + Alpha level, `\(\alpha\)`
  + Critical value
  + Distribution vs. sampling distribution

&lt;br&gt;
.center[
We’ll go over all of these first, and then we'll put them together!

&lt;br&gt;
.red[***This all lays the groundwork for typical experimental research questions*** &lt;br&gt;
(Did the experimental group do significantly better than the control group?)]
]

???

+ Before the slide:
  + Should we reject the null?
  + Is the effect real?
  + Effect means whatever your research question is about

---
#  Chance (or more than chance)

+ The key aspect of hypothesis testing is trying to decide whether differences (or other patterns in data) on some variable are just random chance (so nothing of import is producing the differences or pattern) or are they understandable in terms of some other variable – so that the difference or pattern is not just chance but is “caused” by something else


+ In order to do this, we need to understand what the distribution of a variable would look like by chance alone


+ For this purpose, we often start with the standard normal distribution

???

+ 

---
#  Key terms

+ .red[Null hypothesis], `\(H_0\)`
  + The default assumption; the “status-quo” assumption; there are no differences between means; the opposite of the research hypothesis. “There is no effect.” 
+ .red[Research hypotheses], `\(H_1\)`
  + A.K.A. “alternate hypothesis” 
  + What the researcher proposes to be true. The means are different. the opposite of the null hypothesis. “There is an effect.” 
+ .red[Confidence level], `\(C\)`
  + How certain we want to be, e.g. `\(95\%\)`, or `\(.95\)` (or `\(99\%\)`, `\(.99\)`). Usually decided by the researcher and norms in their field
+ .red[Alpha], `\(\alpha\)`
  + The opposite of confidence level – your willingness to mistakenly conclude there is a real effect even when there isn’t one (e.g., `\(5\%\)` or `\(.05\)`)

.center[Why not pick 99.9999% confidence every time? Then we run a greater risk of “missing” a real effect! More on that later...]

???

+ 

---
#  Key terms

+ An **“effect”** refers to an impact of whatever variable you’re studying
  + Does listening to music make you happier? = Is there an *effect* of music on happiness
  + Did the study group do better on the exams than the control group?  = Is there an *effect* of study on exam scores
  + Are women more empathetic than men? = Is there an *effect* of gender on empathy

???

+ 

---
#  Note on hypothesis testing

+ Note that the null hypothesis and research hypothesis are complementary 
  + Either one of them is true or the other one is! There is no third option


+ Also, alpha and confidence level are complementary: The larger the alpha, the smaller the confidence level
  + The larger the alpha, the more likely we are to make one kind of error (Type I)
  + The smaller the alpha, the more likely we are to commit another kind of mistake (Type II)

???

+ (more on this last point in a few slides)

---
#  Decision table

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;

|                          | `\(H_0\)` is true  | `\(H_0\)` is false |
|:------------------------:|:--------------:|:--------------:|
| **Reject `\(H_0\)`**         | .red[**Type I error**] &lt;br&gt; “False alarm” &lt;br&gt; Probability = `\(\alpha\)`  | .green[**Correct decision!**] &lt;br&gt; Correctly detected a real effect &lt;br&gt; Probability = `\((1 – \beta)\)` |
| **Fail to reject `\(H_0\)`** | .green[**Correct decision!**] &lt;br&gt; Correctly detected no effect &lt;br&gt; Probability = `\((1 – \alpha)\)` | .red[**Type II error**] &lt;br&gt; “Miss” &lt;br&gt; Probability = `\(\beta\)` |

???

+ Whenever we conduct a hypothesis test, there are 4 possible outcomes...shown here
+ In the previous example, the null hypothesis would be that the people who listen to music for one hour are no happier than the general population
+ The research hypothesis would be that people who listen to music for one hour at night ARE happier than the general population
  + “Reject the null hypothesis”
  + “Fail to reject the null hypothesis”
+ Courtroom example here:
  + The process of testing hypotheses can be compared to court trials
  + A person comes into court charged with a crime
  + A jury must decide whether the person is not guilty (null hypothesis) or guilty (alternative hypothesis)
  + Even though the person is charged with the crime...at the beginning of the trial...and until the jury declares otherwise...the accused is assumed to be innocent...innocent until proven guilty
  + Only if there is overwhelming evidence of the person's guilt is the jury expected to declare the person guilty...otherwise the person is considered innocent
  + This is why we say we reject the null or fail to reject null
  + A person could be objectively guilty but found not guilty...think of a person who is guilty but there was no evidence of the crime
      + We did not prove their innocence...we just failed to prove their guilt
  + Likewise we can have an objectively true alternative hypothesis but conclude the null is true
      + We did not prove the null is true in all cases...we just failed to find evidence for the hypothesis
  
---
#  Type I error

+ **Type I: A false alarm**. Concluding there is an effect, and no effect is there. Incorrectly rejecting the null hypothesis when we should have failed to reject it. 
  + The likelihood of committing a Type I error = `\(\alpha\)`
  
.center[&lt;img src="assets/img/image1.png" width=700&gt;]

???

+ 

---
#  Type II error

+ **Type II: A miss**. Concluding there is no effect, but one is there. Incorrectly failing to reject the null hypothesis when we should have rejected it. 
  + The likelihood of committing Type II error = `\(\beta\)`
  + This is the issue of statistical power – a bit more goes into it – we’ll return to this later!


.center[&lt;img src="assets/img/image39.png"&gt;]

???

+ Tip: to remember which one is which, count the “s”s...“False alarm” has 1 s, “miss” has 2
+ 
+ In science, we are typically more worried about Type I errors than Type II errors
  + We don't want to reject the null when it's actually true....this would be the equivalent of making a false claim about the world
  + And as scientists, we don't want to be misleading...our job is to figure out the truth
+ But committing a Type II error is bad too
  + This would mean creating an experiment, potentially spending tax-payers money if we're funded by grants, and making participants waste their time...because a real effect could be there, we're just not detecting it
+ So either way, we want to avoid committing both of these errors! 
+ One is not necessarily better or worse than the other

---
#  Hypothesis testing

+ In all sciences, we start with the assumption the null hypothesis is true 
  + Start out assuming we are wrong


+ Given the null hypothesis, how rare or surprising would our result be? 


+ `\(p &lt; .05\)` means that, if the null hypothesis were true (no effect), you would see the observed data less than 5% of the time
  + We have a result that we are willing to accept as evidence of a “real effect”, because it would be quite surprising (5% of cases or less) to find this result if our effect wasn’t there

???

+ The logic of statistical testing is largely a reflection of the skepticism and empiricism that are crucial to scientific method
+ We start off by assuming the null hypothesis is true
  + In other words...we start by assuming there is no effect of our manipulation or treatment...that we’re wrong
+ Researchers may reject the null hypothesis and conclude that their hypothesis is correct only when findings as extreme as those observed in the study would have occurred by chance alone less than 5% of the time
+ However, even if we find support for the alternative hypothesis...it is always possible that this difference is simply due to chance
+ Statistical hypothesis testing tells us exactly how possible our results are due to chance alone

---
class: inverse
background-image: url('assets/img/image7.png')
background-size: contain

???

+ 

---
class: inverse
background-image: url('assets/img/image8.png')
background-size: contain

???

+ 

---
## Limitations of statistical significance 

+ Significance allows us to make decisions as to whether our findings are due to chance or whether there is something more than chance going on


+ However, it really only answers that one question – *chance, or more than chance*


+ Often there are many other issues that one might like to answer using data from a sample

???

+ So we need another way of using the information we have already learned (measures of central tendency, variability and probability) to use the sample to estimate additional information about the population

---
## Effect size	 

+ More and more, researchers are talking about *effect size*


+ The concept gained traction in the late 1980s
    + Jacob Cohen, 1988
    + Used initially for *meta-analysis*


+ **Effect size** is a simple way of quantifying the difference between groups that has many advantages over the use of tests of statistical significance alone


+ **Effect size** emphasizes the ***size of the difference*** rather than confounding this with **sample size**

.right[&lt;img src="assets/img/image35.jpeg" height=250&gt;]

???

+ 

---
## Effect size 

+ There is another way to look at research results: HOW different are the means?
    + Example: Consider these two (made up) research results:
      + A: “People who go to Loyola are happier than people who go to DePaul, `\(p &lt; .05\)`. Mean happiness for Loyola students is `\(6.3\)`, and mean happiness for DePaul students is `\(6.0\)`.”
      + B: “People who go to Loyola are happier than people who go to DePaul, `\(p &lt; .05\)`. Mean happiness for Loyola students is `\(7.5\)`, Mean happiness for DePaul students is `\(5.5\)`.”

&lt;br&gt;

.right[&lt;img src="assets/img/image36.jpeg" width=350&gt;]

???

+
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
