---
title: "Between vs. Within Subjects Designs"
author: "PSYC 306, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
## Between-subjects designs 

+ Each participant is in only one condition

<br>
<br>
.center[
.pull-left[**Condition 1**]
.pull-right[**Condition 2**]
<img src="assets/img/image4.jpeg" width=400>]

???

+ Just to review a little bit: we’ve already discussed the 2 basic kinds of experiments -- one-way and factorial
    + One-way designs are…
    + Factorial designs have…
+ Now in addition to these 2 types of experiments, there are 2 basic approaches to experiments (and research designs in general): between-subjects and within subjects
    + Between-subject is the approach we’ve discussed so far (participants are in one and only one condition)
    + Within-subject (repeated-measures) – participants serve in more than one (often all) of the conditions
+ Most of the design issues we’ve already covered will apply to within subject designs as well, but we’ll break this down in a bit

---
## Within-subjects designs 

+ Each person is in more than one condition
    + Eliminates person confounds by making each group equal
    + Requires fewer participants

<br>
<br>
.pull-left[.center[**Condition 1** <br><br>
<img src="assets/img/image5.png" width=250>
]]

.pull-right[.center[**Condition 2** <br><br>
<img src="assets/img/image5.png" width=250>
]]

???

+ Each participant is in more than one condition (often all of them)
+ Since everyone in the sample is in every condition, we do not need to worry about making 2 groups
+ Also, by having everyone in the same group, each participant essentially serves as his or her own control (so we don’t have to worry how equal the groups are at the beginning of the experiment)
+ Since the participant is in every condition, we can see how this specific person’s scores change between conditions
+ This is because the factors in a within-subject design do not have to “fight” all the noise occurring between conditions due to individual differences
+ Because the noise is reduced, it makes it easier to detect effects if they exist
+ Recall that we usually get rid of individual differences by using random assignment
+ We don’t have to worry about random assignment with these designs
+ When we use within subjects, we’re getting rid of individual differences in a more direct fashion
+ Because each person is serving in every condition, we’re essentially using the only perfect form of matching
    + We use the exact same person in each condition
    + This completely eliminates any individual differences between conditions
+ We still need more than one person in order to generalize to all people
+ We also need variability in scores to run statistics 
    + Usually need an absolute minimum of 5-10 using within subjects, need at least 25+ when using between subjects
    + But there are statistical tests you can run to find out how big of a sample you need
      + Called power, we'll discuss more in the stats unit
+ Requires fewer participants
    + e.g., 3 x 3 x 2 needs 18 conditions
    + If we needed 50 participants per cell, we would need 900 participants in a between-subjects design
    + By using a within-subjects design, we only need 50 participants

---
## Between- vs. Within-subjects

<br>
.center[<img src="assets/img/image6.jpeg" width=700>]

???

+ Casual vs. formal (expert vs. novice)
