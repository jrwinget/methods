---
title: "Final Paper Formatting"
author: "PSYC 306, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
##  General formatting 

+ The paper should be in Times New Roman, 12pt font


+ The entire paper should be double spaced


+ Use 1 inch margins on all sides

???

+ 

---
##  APA paper overview 

+ Title page


+ Abstract


+ Introduction
  + *Draft already written*


+ Method
  + *Draft already written*


+ Results
  + *Draft already written*


+ Discussion
  + *Draft already written*


+ References


+ Figure(s)

???

+ 

---
##  Title page 

+ Title should provide a description of the paper’s topic
	+ 10-12 words


+ Author affiliation


+ Running head
  + Goes in the header of the word document
	+ Maximum of 50 characters
	+ On title page only, this is preceded by the words “Running head:”
	+ All caps

???

+ 

---
##  Abstract 

+ Maximum of 250 words


+ Not indented


+ Provides a brief summary of the contents of the paper
  + Introduction, methods, results, etc.

???

+ 

---
##  Main text 

+ Introduction has no heading
	+ Center the title of the paper at the top of the page


+ Method, Results, and Discussion each have a heading and continue on the same page as the preceding section
	+ Do not leave headings alone at the bottom of the page

???

+ 

---
##  References 

+ Starts on its own page


+ Each reference uses a hanging indent
  + First line is flush with the margin, any remaining lines are indented


+ See examples for proper APA style

???

+ 

---
##  Figures

+ Main effects will be a bar graph
	+ Interaction will be a line graph


+ Should have a caption below that fully explains what it shows
  + e.g., "Figure 1. Statement of main effect"


+ See examples for formatting

???

+ 

---
##  Purdue OWL (online writing lab) 

+ General format
	+ https://bit.ly/2uyze7X


+ Sample paper
	+ https://bit.ly/2AKB4Im
