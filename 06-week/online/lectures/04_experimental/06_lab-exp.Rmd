---
title: "Conducting a Lab Experiment"
author: "PSYC 306, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
## Conducting lab experiments 

+ Pilot test
    + Make sure the experiment runs smoothly


+ Manipulation check
    + Make sure the independent variable was manipulated as intended
    + Check the realism of the experiment


+ Deception
    + Usually increases experimental realism

.right[<img src="assets/img/image15.png" height=300>]

???

+ That said, experimental realism can only help us when we can be certain the participants in a study do in fact experience the psych states the experimenter hopes to create
+ This, of course, can be difficult
+ Luckily, however, researchers have figured out some best practices for inducing experimental realism
+ We talked about this first point a bit last class: Pilot testing
    + By pilot testing our study first, we can figure what does (and doesn't) run smoothly
    + Measures, procedures, participants' ability to guess what's going on, etc.
+ Manipulation check
    + Allows to check exp realism of study by directly asking participants
+ Deception
    + Cover story
    + By convincing participants they are taking part in one kind of study (e.g., preference task), can often get more accurate answers for your real/intended study (e.g., open-minded cognition)
    + Doesn't have to be elaborate (it can be), but they're often small, passive lies (e.g., failing to tell participants about the precise reasons they study is being conducted)
    + Must debrief at end of the study
    + It's about creating convincing illusions that cause people to experience the precise situations you care about as a researcher

---
## Manipulation check 

+ Example: Investigating effects of stress on problem solving
    + After manipulation or at end of procedure, give participants a questionnaire assessing stress or take their blood pressure

<br>
<br>
<br>
<br>
<br>
.right[<img src="assets/img/image14.jpeg" height=300>]

???

+ Even after the development phase is finished, there are still ways we can check for experimental realism in our participants
+ If you're not sure your manipulation is doing the trick, you can ask your participants what they think
+ By including manipulation checks, researchers can see if a manipulation really put participants in the psych state that was intended
+ For example: if trying to manipulate attractiveness, could ask participants to rate attractive people at end of the study

---
## Trade-offs

+ Lab studies tend to be high in internal validity while field studies tend to be high in external validity
    + This is not always the case

???

+ While there are usually trade-offs between internal and external validity, it’s not as large of a trade-off as one may think
+ We will dive more into this in a couple weeks when we talk about threats to validity
+ Generally speaking, laboratory studies tend to be higher in internal validity
    + Extent to which IV causes changes in DV
    + Can be more confident about his in the lab
    + Random assignment, manipulation, reduced noise, confounds eliminated
    + Threats to generalizability
+ Field studies tend to be higher in external validity
    + Extent to which findings from sample generalize to population
    + Conducted in the "real world"
    + Threats to causation
+ However, they are not mutually exclusive
    + For example we can have a lab study that induces a particular emotion
    + In the lab we can ensure high internal validity
    + And that internal psych state (that emotion) is likely to generalize outside of the lab

---
## Trade-offs 

+ Experimental realism increases external validity


+ Carefully designed field studies can be high in internal validity


+ Field studies often help bolster findings from a lab study

???

+ Explain slide

---
## When done, replicate! 

+ Results are more convincing if they are replicated
    + Use other operationalizations of your IV and DV

.center[<img src="assets/img/image16.jpeg" height=450>]

???

+ Studies are always more convincing if you replicate the findings
+ After all of this careful and deliberate work, you might be convinced you've nailed down your measures/design
+ But there's always a chance your results are a fluke 
    + Could be a statistical chance
    + Could be due to the very specific conditions you created
+ In fact, it's probably accurate to say that almost all hypotheses are both correct and incorrect (under different conditions)
+ Using other manipulations and measures in multiple studies not only replicates your study, it makes your findings more generalizable 
    + Now we know that it’s not just one manipulation or one set of measures or one particular sample that brings about the effect
+ On the other hand, if the experiment comes out "wrong", the researcher should conclude there was something wrong with the experiment/theory
    + Check your sample size, strengthen the IV manipulation, by of extraneous response possibilities, or set up a more appropriate context
    + Try to learn *why* it didn't work
    + Perhaps it's a boundary condition of the theory
+ Despite how important replication is to the scientific enterprise, you might be surprised to find out historically psychology has failed to do this
+ Only in the last decade or so have replication attempts increased
    + And many have them have not been kind to the published literature
+ We’ll talk more about what's been called the reproducibility crisis and the subsequent open science movement in psychology at the end of the semester
