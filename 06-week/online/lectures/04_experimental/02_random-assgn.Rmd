---
title: "Random Assignment"
author: "PSYC 306, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
## Random assignment activity 

+ Methodology exercise 2 in the book—page 492
    + Complete the activity and answer the questions

--

<br>
<br>
+ Overall averages (grand means):
  + **Age:** 24 (*SD* = 6.62)
  + **IQ:** 100.55 (*SD* = 23.54)
  + **Height:** 69.1 (*SD* = 3.81)
  + **Surgency:** 7 (*SD* = 2.43)
  + **Self-esteem:** 39.7 (*SD* = 4.94)


???

+ Work individually, then we'll come back as a group to discuss
+ State the grand means
  + Surgency is the tendency to feel high levels of affect/emotion
+ Did your two groups end up more similar to or different than one another?
+ Did random assignment seem to work better for some variables but not others 
    + Think about variability
    + If we have a difference in IQ of 101 and 107, is that the same as a 6-point difference in age?
+ What do you think would happen to our two groups if we randomly assigned 40 people rather than only 20 like we did? 
    + Would groups be more similar, more different, or no effect? 
      + By allowing more participants to have an equal chance of being assigned to the exp or control group, there is more variability that can be equalized on average
      + This has to do with the central limit theorem we talked about in the first lecture this semester
    + Main idea is that the more people we have, the more equal our groups become during random assignment
+ The other great thing about random assignment, as you can see here, is that it doesn't just equate groups on the particular dimensions we care about
    + It essentially equates group on *every* dimension imaginable (especially with a very large sample)

---
## Random assignment vs. Random selection 

+ Differences
    + Random assignment equates experimental conditions before the manipulation
      + Helps with internal validity
    + Random selection increases the chances results will generalize to a population
      + Helps with external validity


+ Similarity
    + Both increase similarity between two groups of people

???

+ Before we go on, it's important to compare and contrast random assignment with random selection (aka, random sampling)
+ These are very different approaches, but they essentially tackle the same problem
+ Random selection
    + Used in studies to help increase the likelihood the sample in a study generalizes to the larger population of interest
    + Has to do with external validity (extent to which findings generalize)
+ Random assignment
    + Used in experiments to help equate different experimental conditions before manipulations are applied
    + Has to do with internal validity (extent to which IV causes changes in DV)
+ Both of these forms of randomization maximize the likelihood 2 separate groups of people will be as similar as possible
+ Only different has to do with the groups of people you're worried about
    + If you want to be sure people in the study are like the people in the real world, pull people from the real world at random to participate in your study (random selection)
    + If you want to be sure people in one if the experimental groups are exactly like the people in another group, randomly assign them to a condition (random assignment)
