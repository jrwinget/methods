---
title: "The History of Ethical Guidelines"
author: "PSYC 306, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
## Ethics of scientific discovery 

+ Psychologists must consider ethics when designing experiments
    + Deception
    + Sensitive topics
    + Special groups

.right[
<img src="assets/img/image10.jpeg" height=300>
]

???

+ Since social science uses humans for our research participants, there are many ethical issues we need to consider before ever conducting a study
+ The truth is some experiments (despite how interesting they might be) cannot be tested in the most informative or creative manner
    + For example, if you have a theory about the conditions under which genocide is likely to occur, you cannot create an experiment and ask people to participate in something that might cause them harm
    + This might be very important information to know, but you cannot put your research subjects in harm’s way.
+ This should be pretty obvious, but there are more mundane theories that can pose ethical challenges, and these challenges may not be as obvious as the genocide example
+ When conducting experimental research, it’s often necessary to distract or deceive participants to get them to behave naturally, which can be an ethical gray area at times
+ In social psychology, we study some of the most sensitive topics there are to study
    + Things like aggression, depression, drug possession, romantic obsessions, morality, or prejudice are extremely important topics to study, but they are some of the most ethically controversial to study as well
+ In developmental and clinical psychology, researchers must also consider the ethics of studying special groups that may not be competent to give informed consent to participate in studies (e.g., young children, people with schizophrenia, prison populations etc.)
+ Even in behavioral neuroscience, researchers much consider about whether and when it is appropriate to cause nonhuman animals pain to study important questions
+ So, in the rest of today’s lecture, we’ll cover some historical examples of studies that have raised important ethical questions and discuss some of the guidelines that have been put forth to help protect research participants

---
## History of ethical guidelines 

+ Tuskegee syphilis study (1932-1972)
	+ US Public Health Service
	+ Effects of untreated syphilis
	+ Subjects were African-American men from rural Alabama
	+ Not given penicillin after it was found to cure syphilis

???

+ In the early days of research with human participants, there weren’t any real guidelines in place concerning the rights of human participants or the responsibilities of researchers to their participants
    + At the time, things were basically accepted on faith. Researchers just trusted other researchers’ judgment about these matters and thought no one would let anything bad happen.
+ This (surprise, surprise) was a misguided approach. 
+ In 1932, the Tuskegee study began
    + Researchers wanted to learn about the effects of untreated syphilis (which if left untreated, eventually leads to insanity and death). 
    + In the early days of the study, there was no known cure, so it didn’t violate any ethical rules
    + In 1943, researchers discovered penicillin cures syphilis
    + But instead of treating them, researchers specifically ordered doctors not to treat their patients
    + When some of the men were drafted for WWII, researchers broke state/federal law to keep them from treatment that would’ve been given during their med exams for draft
    + It’s also probably not a coincidence the patients in the study were 600 rural Black men (while the researchers were all White)
+ In 1973, Congress held hearings on the Tuskegee experiments
    + Following year the study’s surviving participants, along with the heirs of those who died, received a $10 million out-of-court settlement
    + Additionally, new guidelines were issued to protect human subjects in U.S. government-funded research projects
+ This was a medical study, not psychological
    + National attention led to the creation of a set of ethical standards for psychological research with human participants
      + Generally ignored

---
## History of ethical guidelines 

+ Milgram (1963)

.center[
<iframe width="560" height="315" src="https://www.youtube.com/embed/mOUEC5YXV8U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
]

???

+ I’m sure many of you have heard about the Milgram studies, but this now famous study marks a significant point in the history of ethical guidelines
+ Although the first set of ethical guidelines was published in 1958, psychologists didn’t pay much attention to them
+ It wasn’t until the Milgram studies came about that they started paying more attention to these issues
+ We're going to watch some of the original footage from the Milgram experiments
    + Try to look for any ethical problems you see
    + The link is posted on Sakai...so go ahead and pause this video and check that one out
    + I'll see you when you get back
+ 
+ What ethical problems did you see? 
    + Deception (necessary and participants were debriefed)
    + Emotional distress (most participants reported feeling that they had learned something important and were glad they participated) 
    Coerced into continuing (this was the point of the study) Current day IRB 
+ Modern replications of this experiment have occurred, but with modifications 
    + Shocks go up to 150 volts—this is when the learner vehemently protests and is a critical point in whether or not participants will continue
    + Extensive screening of participants and stopped if they were too distressed
    + Made it very clear that they could stop any time and still be paid

---
## History of ethical guidelines 

+ Humphreys (1970)
    + What motivates people to engage in sexual activities in public restrooms?
    + Observational research
    + Recorded automobile license numbers
    + People did not know they were being observed
      + Did not consent to being in the study

???

+ Though psychologists started paying attention to ethics in the wake of the Milgram studies, there was another infamous study ridden with ethical issues that was conducted just a few years later, but this time in sociology
+ Humphreys conducted dissertation research on men who have anonymous sex with men
+ The purpose of the study was to dispelling myths that the men he studied were dangerous social deviants
+ He found that most were married to women and had children; and only a small percentage identified themselves as gay
+ So, he was able to successfully despite many stereotypes and myths. 
+ Study called into question some of the stereotypes associated with the anonymous male-male sexual encounters in public places
    + Demonstrating that many of the participants lived conventional lives, had families, were respected members of their communities
    + 54% outwardly heterosexual with unsuspecting wives
+ Many within the gay community, at the time, welcomed his research, and in some police districts, it lead to decreased raids and sodomy arrests
+ However, there are still issues with how this study was conducted
+ Without disclosing his role as a researcher, Humphreys played the role of “watchqueen,” which meant he looked out for intruders while men engaged in homosexual acts in public restrooms of parks
    + He lied to participants by saying he just liked to watch, and because of this, was permitted stay in the bathroom at these times
    + Among other things, he gathered data on locations, the frequency of acts, the age of the men, the roles they played, and whether money changed hands
    + In other cases, he recorded his subjects’ license plate numbers to track where they lived
+ So, he lied about his identity, did not get consent from participants, and collected identifying information from them
    + All of which are major violations of participants' rights

---
## History of ethical guidelines 

+ The Belmont Report (1979)
	+ Basic ethical principles:
		+ Respect for persons
		+ Beneficence
		+ Justice

.right[
<img src="assets/img/image14.jpg" height=300>
]

???

+ In response to these growing concerns of participant safety in research, The Belmont Report was published
+ The Belmont Report was a major step forward in the history of ethical guidelines
+ It summarized the ethical principles and guidelines for research involving human participants
+ The 3 core principles are…
+ These principles help protect participants and ensure research responsibility when ethical issues arise
+ So let’s break them down a bit…

---
## History of ethical guidelines 

+ The Belmont Report (1979)
	+ Respect for persons:
		+ Individuals should be treated as autonomous agents
		+ Persons with diminished autonomy are entitled to protection
			+ E.g., children, mentally disabled, prisoners

.right[
<img src="assets/img/image15.jpg" height=300>
]

???

+ The first core principle is Respect for Persons
+ Concept that all people deserve the right to fully exercise their autonomy
+ Showing respect for persons is a way of interacting with a participant that ensures that they have their agency to be able to make a choice
+ It comprises two essential moral requirements
    1. To recognize the right for autonomy
    1. To protect individuals who are disadvantaged to the extent that they cannot practice this right
+ An autonomous person is defined as an individual who is capable of self-legislation and is able to make judgments and actions based on their particular set of values, preferences, and beliefs
+ Respecting a person’s autonomy thus involves considering their choices and decisions without deliberate obstruction
+ It also requires that subjects be treated in a non-degrading manner out of respect for their dignity
+ In practice, respect for persons is operationalized by obtaining Informed Consent from all individuals who are going to be research subjects

---
## History of ethical guidelines 

+ The Belmont Report (1979)
	+ Beneficence:
		+ Do no harm
		+ Minimize harm and maximize benefits
		+ Risk-benefit ratio

.center[
<img src="assets/img/image17.png" width=400>
]

???

+ The second core principle is Beneficence
+ Concept which states researchers should have the welfare of the research participant as a goal of any clinical trial or research study
+ The concept that medical professionals and researchers would always practice beneficence seems natural to most patients and research participants, but in fact, every health intervention or research intervention has potential to harm the recipient
+ Just like there are potential harms we encounter in everyday life; there is no escaping them
+ Some harms are greater than others, so a cost/benefit analysis is often applied on an individual study by study basis
+ There are many different methods in research for conducting a cost–benefit analysis and judging whether a certain action would be a sufficient practice of beneficence, and the extent to which treatments are acceptable or unacceptable is still under debate
+ Despite differences in opinion, there are many concepts on which there is wide agreement
+ One is that there should be community consensus when determining best practices for dealing with ethical problems

---
## History of ethical guidelines 

+ The Belmont Report (1979)
	+ Justice:
		+ Some benefit to which a person is entitled should not be denied
		+ A burden should not be unduly imposed

.right[
<img src="assets/img/image18.jpg" height=275>
]

???

+ The third core principle is Justice
+ Concept that involves ensuring reasonable, non-exploitative, and well-considered procedures are administered and equally.
+ When I say fair, I mean the distribution of costs and benefits to potential research participants is about equal. 
+ This basically means we can’t have our participants jumping through extensive hoops and compensating them poorly. 
    + The risks they’re subjected to in the study cannot outweigh their benefits
+ This idea can also be extended to the fair selection of research participants
+ It provides guidelines on how research objectives (rather than membership to a privileged or vulnerable population) should determine the inclusion/exclusion criteria to participate in research.

---
## History of ethical guidelines 

+ APA guidelines
+ Institutional Review Board
	+ Risk-benefit analysis

.right[
<img src="assets/img/image19.jpeg" width=350>
]

???

+ In addition to The Belmont Report, there are other ethical guidelines as well
+ For psychologists, we also need to be aware of the ethic guidelines published by the American Pscy Association
+ APA guidelines continue to be updated, but then general principles remain the same
+ You might be wondering how these guidelines are enforced because as we’ve seen throughout history, researchers haven't been the greatest at policing themselves
+ So, this is where Institutional Review Boards (IRB) come in
+ All research at American universities must first be approved by an IRB
+ These review boards review and evaluate all research that use human participants
+ They differ slightly by institution, but they all have the same purpose
    + The preform the risk/benefit analysis to ensure all studies meet consensual community standards of ethical behavior
+ IRB consists of
    1. A group of instructors and researchers as the institution, 
    1. One or more university staff members with special expertise in the area of research protocol
    1. One or more lay people from the local community (e.g., businessperson, minister who is active in community service)
+ Researchers must submit a formal proposal to the IRB each time they want to conduct a study involving human participants
