<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>The Response Translation Phase</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 306, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# The Response Translation Phase
### PSYC 306, Winget

---

## Responding to a survey 

+ Step 1: Judgment phase


+ Step 2: Response translation phase

&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image11.jpg" height=350&gt;]

???

+ So we've already talked about the initial judgment phase
    + We covered a lot in that video
    + But that was basically the process of eliciting the correct internal psychological state we wish to place our participants in
+ However, to make sure that we’re collecting accurate data, we need to ensure participants are able to translate this internal psychological state into some kind of value on a response scale 

---
## Response translation phase 

+ Participants must translate their internal state into a response on the scale provided

&lt;br&gt;
&lt;br&gt;
.center[
&lt;img src="assets/img/image4.jpeg" height=225&gt;
&lt;img src="assets/img/image5.jpeg" height=225&gt;
]

???

+ Basically, participants need to be able to accurately communicate things about their internal state to the researchers through the researchers’ measures
    + This is called the response translation phase
+ In this phase, researchers are concerned with 3 general questions
    + *Can* participants translate their internal psychological state into some kind of value on a scale?
    + *Can* participants identify a reasonable value?
      + Is there even an option on the scale for their internal state?
    + *How* well can they do it?
      + Does the participant have to choose between choices?
      + Does this value actually reflect their true state (e.g., agree to disagree, usually middle option is neither agree nor disagree, but what’s the difference between someone who has mixed feelings vs. someone who doesn’t know at all)

---
## Important factors 

.pull-left[
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
+ Number of scale points


+ Anchors
    + Endpoints and midpoints
    + Unipolar vs. bipolar
    + Equal intervals
]

.pull-right[
&lt;img src="assets/img/image6.gif" height=400&gt;
]

???

+ When designing a numerical scale, there are at least 2 things to consider
    1. How many numbers to use
        + 1-3, 1-5, 1-10
    1. Which anchors to use
        + (strongly) agree/disagree, (strongly) like/dislike
        + Only on the ends? What about the middle point?
        + 1-7 or -3 to 3
        + Distance between units is equal, difference of 1 and 3 should be the same as the difference between 4 and 6 (including the internal state distance between agree–mid and mid–disagree)
+ There can be more issues depending on the research goal
    + e.g., depending on the question, researchers might need to decide on the numbering system to use in the scale 
    + See the "special cases special scales section" of the chapter for more details on this
+ But these two are essential questions that will apply to any goal
+ Will explain these concepts in more detail over the following slides

---
## Numbers on a scale 

+ How many should be used?
    + *How satisfied are you with your life right now?*

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image12.png" width=800&gt;]

???
+ The first thing to consider is how many numbers you want to have on the scale
+ Most psychological scales use 1-7, but you could have just about any number you'd like
    + So, how do you decide which is best for your scale?
+ Well, as with most psych measurement, it's tricky
    + You want to have enough to reflect a wide variety of options, but not so many as to cause confusion
    + Because different questions involve different responses, there is no single answer for how many response options are in an ideal rating scale
+ There are some things to consider though
+ Fewer numbers risk being too **CRUDE** 
    + Participants will probably be able to make a decision, but the measurement is likely forcing them in a direction that is more extreme than their actual internal psychological state
    + Plus, we don’t get much detail from the data if there are only a couple of options for participants to choose from
+ Too many numbers risks paralyzing participants with **INDECISION** or failing to get a **PRECISE MEASUREMENT**
    + Imagine if we stretched the scale from 1-68, it would probably be tough to find a number that participants are satisfied with or that actually captures their true state (because the participants might over think it or second guess themselves)
+ It’s usually better to have more than 2 or 3 options on a scale because you can get more details from the data
    + For example, someone who is moderately or only somewhat satisfied with their life can indicate that
    + But having too many options on a scale starts to become a matter of splitting hairs
+ However, this can vary by the purpose of the measurement too
    + For **CODING** scales, I tend to use fewer #s because I want EACH UNIT to correspond to a PARTICULAR ATTRIBUTE that we are trying to code
      + For example, in some of my research, we record group discussions where we ask the group to make a decision, and we might code to see if they mentioned anything related to self-interested behavior in general
      + We usually have 3 response options: self-interest, interest of other group, equality of groups
    + On the other hand, for a **SELF REPORT** survey about a JDM, I might use more ?s because people are better able to classify HOW CONFIDENT they are about a particular decision
      + They're not simply saying whether or not they are confident
      + They're responding to a series of items about their JDM
+ Another thing to consider is whether there will be a total number of even or odd responses in the scale
    + If you choose to use **EVEN** numbers (such as 1-4), there will not be a middle point
      + This forces participants to decide which way they’re leaning when asked a question
      + Sometimes, this can be preferable if you want to assess something that many people might take a middle stance on, but again, this starts to force the participants in one direction
      + Thus, anyone who might not have any opinion or who might truly be in the middle cannot accurately translate their internal state
    + If you choose to use **ODD** numbers (such as 1-7), there will be a middle point
      + This gets around the issue of forcing participants in one direction or another, but carries the potential to collect data with reduced variance 
      + Especially if many people gravitate towards the middle for social desirability reasons
+ Overall, psychometricians (experts in scale construction and pscyh measurement) recommend most psych constructs should have an intermediate level of responses
    + Typically between 3 &amp; 10, but there are exceptions (e.g., sex, did you vote in the last election - y/n)

---
## Anchors on a scale 

+ Anchors should lend meaning to the numbers on a scale

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image13.png" width=900&gt;]

???

+ The question of how many response options to include on a scale is somewhat related to the second question of what kind of anchors to use on a scale
+ Anchors are adjectives (or short strings of words) that give meaning to the numbers on a scale
+ If we didn’t include them, participants wouldn’t know what the high vs low end of the scale was, and they would probably have a tough time interpreting the question in the way the researchers would like them to
    + One person might assign a strong negative anchor to 1 (abysmal) whereas another person might see 1 as strongly negative but less negative the first person (very bad, but not abysmal)
    + So it’s important researchers really think about the anchors they choose because changing the anchors on a scale can have big effects on people’s answers
+ DISHONEST RESEARCHERS can, of course, purposefully manipulate the outcome of their research by changing the scales or anchors, but such biasing MAY ALSO BE TOTALLY UNINTENTIONAL
+ Either ONLY THE TWO ENDPOINTS of the scale are "anchored“
    + or else EACH RESPONSE CATEGORY IS LABELED 
+ ENDPOINTS are very useful, but still leave room for issues to creep in
    + The areas around 3-5 become hard to interpret: is the middle a continuum from one end to the other, or is the the undecided option?
    + This scale would be a bit easier to use if it contained MIDDLE ANCHORS
+ If EACH RESPONSE CATEGORY IS LABELED, this gives the scale much more meaning and makes it easier for the participant to interpret the question
    + However, researchers need to make sure the labels in between conserve equal intervals
    + A scale has equally appearing intervals whenever the psych distance implied by a single unit difference on the scale remains constant across the scale
      + In other words, the difference between the labels is roughly the same for each step up in the scale
    + This is usually easy with WORD STRINGS (a moderate sense of exclusion v. belonging), but can becomes tricky with ADJECTIVES because they often have an inherent sense of an psych internal state attached to them
      + e.g., people generally consider "extremely" to be more than "very", and "quite" more than "somewhat"
+ Because of this, we need to make sure the intervals between numbers are PSYCHOLOGICALLY EQUIVALENT
    + 1 vs 3 should be the same psychologically as 4 vs 6
    + Is the distance between "slightly" and "quite" the same as the distance between "mostly" and "very"?
+ Figuring out how to create psych rating scales with equally appearing intervals may seem difficult if not impossible
+ But, by defining equally appearing intervals precisely (i.e., operationalizing), it’s actually an achievable goal
+ And because we can operationalize it, there’s been research showing that participants rank order certain words by their implied magnitude in a consistent pattern
+ It turns out, words like “not” “slightly”, “quite”, and “extremely” have roughly 0.5 units apart from one another in these studies
+ So we tend to use these anchors across most psychological scales because past research supports their use
+ Now, although this second scale is in a format that could be used for most questions with a little tweaking, some psychological constructs are better captured with bipolar scales

---
## Unipolar vs. bipolar 

+ Depends on what is being measured
    + *How satisfied are you with your life right now?*

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image14.png" width=800&gt;]

???
+ Pretty much all the scales we’ve discussed this far have been unipolar
    + In those cases, the scales have begun at a low value (0 or 1) and move upward to a subjective maximum point on the dimension of interest
+ But, what if we wanted the life satisfaction scale to be more sensitive to the possibility of extreme dissatisfaction? 
    + With a unipolar scale, participants could only indicate a lack a satisfaction, not outright dissatisfaction
+ Well, we could use what’s called a bipolar scale
    + Bipolar scales ask respondents to rate a quantity that deviates in both directions from a zero point
+ Unipolar scales are good if you want to measure where something like an attitude is from weak to strong
+ Bipolar would be better if you thought the attitude had a bit of DUALITY to it (e.g., like/dislike)  
+ You might notice bipolar scales are a bit limited if we only stick to 7 items
    + However, people tend to treat bipolar scales a bit differently when responding to them
+ WHEN ANSWERING
    + People usually break their responses on bipolar scales into TWO PARTS
    + First, participants decide where they fall in relation to the midpoint (in this case, whether to go BELOW or ABOVE the zero)
    + Second, they decide where they fall on the RESTRICTED RANGE of the side they choose (only focus on positive or negative side of scale)
    + Thus, participants essentially limit themselves to a scale with three points in this case
      + Unless they where neither, in which case they would select 0
+ So even though psychometricians recommend using scales with 3-10 points, this rule is often modified in the case of bipolar scales
    + Because participants often break up the scale into 2 parts, they are really only evaluating half of it
    + Thus, if a bipolar scale has 13 points, participants will really only be focusing on 7 of them
+ UNIPOLAR VS BIPOLAR IS ABOUT THE CONCEPT, NOT THE NUMBERS

---
## Match the question to the scale 

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.pull-left[
1. Do you enjoy college?
&lt;br&gt;
&lt;br&gt;
1. How favorable or unfavorable has your college experience been?
&lt;br&gt;
&lt;br&gt;
1. To what extent do you enjoy college?
]

.pull-right[
&lt;ol type=A&gt;
&lt;li&gt;7 point scale from 1 (“not at all”) to 7 (“extremely”)&lt;/li&gt;
&lt;br&gt;
&lt;li&gt;Yes or No&lt;/li&gt;
&lt;br&gt;
&lt;li&gt;100 point scale from -50 (“extremely negative”) to 50 (“extremely positive”)&lt;/li&gt;
&lt;/ol&gt;
]

???

1. B
1. C
1. A
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
